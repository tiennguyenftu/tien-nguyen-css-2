$(document).ready(function () {
  $('#about-me').waypoint(function (direction) {
    if (direction == 'down') {
      $('nav').addClass('sticky');
    } else {
      $('nav').removeClass('sticky');
    }
  }, {
    offset: '60px'
  });

  // $('.main-nav li a').click(function () {
  //   $('.main-nav li a').removeClass('active');
  //   $(this).addClass('active');
  // });

  $('.mobile-nav-icon').click(function () {
    var nav = $('.main-nav');
    var icon = $('.mobile-nav-icon i');
    nav.slideToggle(200);
    if (icon.hasClass('ion-navicon-round')) {
      icon.addClass('ion-close-round');
      icon.removeClass('ion-navicon-round');
    } else {
      icon.addClass('ion-navicon-round');
      icon.removeClass('ion-close-round');
    }
  });
});